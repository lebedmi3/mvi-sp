# MVI - Semestrální práce

## Milestone
[MILESTONE.md](MILESTONE.md)

## Soubory
- [main.ipynb](main.ipynb) - Notebook s tvorbou, trénováním a vyhodnocením (na validační množin1ě ) modelů
- [evaluation.ipynb](main.ipynb) - Notebook s analýzou výsledků na validační množině, vizualizacemi pro report a vyhodnocení nejlepšího modelu na testovací množině.
- [report.md](report.md) - Report v Markdown formátu
- [report.pdf](report.pdf) - Report v PDF formátu

## Instrukce
``pip install -r requirements.txt``
