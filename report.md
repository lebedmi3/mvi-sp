\hfill Michal Lebeda (lebedmi3)

# Adding Convolutional Layers Before ViT

## Introduction

This project is focused on experimentally prepending convolutional layers to Vision Transformer[1].
The main goal is to achieve faster training times by lowering ViT input size.

Convolutional layers may successfully compress input data and allow usage of pooling layers significantly lowering input data size.

## Models

9 different models were evaluated, consisting of 5 different ViT configurations and 4 more configurations of ViT with prepended convolutional layers. Vit-pytorch[2] implementation was used as a starting point.

### ViT Configurations

Experiments on using different ViT parameter values were conducted.

- Parameters:
    - `patch_size` - Size of patches image is split into before reaching transformer network.
    - `dim` - Last dimension of the output tensor after linear transformation.
    - `mlp_dim` - Dimension of the dense layer of ViT.

Used ViT configurations:

| patch_size |  dim | mlp_dim | benchmark | experiments                                                     |
|-----------:|-----:|--------:|:---------:|-----------------------------------------------------------------|
|          2 |   32 |      64 |    yes    | test influence of `patch_sizes`                                 |
|          4 |   32 |      64 |    yes    | test influecne of `patch_sizes`                                 |
|          4 |   64 |     128 |           | test influence of different dimension counts                    |
|          4 |  128 |     256 |           | test influence of different dimension counts                    |
|          4 | 1024 |    2048 |           | test *extreme*(proportionally to the used data) dimension count |

Some of these configurations were used as a direct benchmark of the custom network as they consist of the same ViT configuration.

### Custom Network

2 configurations of convolutional layers were tested. Every convolutional layer use `3x3` kernel and doubles the input channels on the output.
Zero padding of size `1` was applied in order to compensate the kernel size.
Max pooling was applied after convolution followed by ReLU layer.

- **2xConv**
    - `Conv2d(in_channels=3, out_channels=6)`
    - `MaxPool2d((2, 2))`
    - `ReLU()`
    - `Conv2d(in_channels=6, out_channels=12)`
    - `MaxPool2d((2, 2))`
    - `ReLU()`


- **3xConv**
    - `Conv2d(in_channels=3, out_channels=6)`
    - `MaxPool2d((2, 2))`
    - `ReLU()`
    - `Conv2d(in_channels=6, out_channels=12)`
    - `MaxPool2d((2, 2))`
    - `ReLU()`
    - `Conv2d(in_channels=12, out_channels=24)`
    - `MaxPool2d((2, 2))`
    - `ReLU()`

### Custom Network Configurations

Configurations of the ViT part of the custom networks were chosen more modestly as the goal is to decrease training/inference time.
The networks contain prepended convolutional layers as described above.

Complete table of custom network variants:

| conv. variant | patch_size | dim | mlp_dim |
|--------------:|-----------:|----:|---------|
|        2xConv |          2 |  32 | 64      |  
|        2xConv |          4 |  32 | 64      |  
|        3xConv |          2 |  32 | 64      |  
|        3xConv |          4 |  32 | 64      |

## Data

As the available HW was not able to perform training on a huge dataset, smaller Cifar-10 [3] dataset was used.
The dataset consists of 50000 training and 10000 test samples. 5000 samples from train set was further reserved for validation and not used for training
Dataset consists of images of resolution `32x32`, meaning the commonly used ViT configuration will be overkill.
Despite that, one particularly *"heavy"* configuration was trained and evaluated as an experiment.

Sample from Cifar-10 dataset:  
![cifar_sample.png](assets%2Freport%2Fcifar_sample.png){width=400px}



## Training
Each network was trained using `SGD` and `AdamW` optimizer.  
The training process in each case created a slightly different network weights for each optimizer, thus each variant was evaluated separately.

Training was done (in case of both optimizers) with the following hyperparameters:

- learning rate: `0.001`.
- batch size: `256`.
- epochs: `30`
- class count: `10`
- train samples: `45000`
- validation samples: `5000`

In general, `AdamW` was quicker in terms of convergence while `SGD` was much slower:

![](assets/img/ViT_(patch_size=2,_dim=32,_mlp_dim=64)_AdamW.png){width=320px}
![](assets/img/ViT_(patch_size=2,_dim=32,_mlp_dim=64)_SGD.png){width=320px}

On the other hand, `AdamW` was more prone to over-fitting:

![](assets/img/ViT_(patch_size=4,_dim=128,_mlp_dim=256)_AdamW.png){width=320px}
![](assets/img/ViT_(patch_size=4,_dim=128,_mlp_dim=256)_SGD.png){width=320px}

It is necessary to point out that the `AdamW` started to overfit during train loss (and even val. loss) values not reached by `SGD`.

## Results

This results section consists of parameter experimentation and comparison between modified ViT and original version.

### Validation Loss

**NOTE:** Parameter names were shortened in order text to better fit images:

- `p == patch_size`
- `d == dim`
- `m == mlp_dim`

Validation results of all models:  
![](assets/report/validation.png){width=600px}

Validation results of custom and original network pairs only for better comparison:  
![](assets/report/validation(benchmark_only).png){width=600px}

Custom models scored similarly to their original counterparts, yet a bit worse. Bigger models did not perform that well, especially the biggest one.

The "`d=1024`" model started to diverge using `AdamW`. Using `SGD`, the model simply failed to converge fast enough as can be seen on the following images:  
![](assets/img/ViT_(patch_size=4,_dim=1024,_mlp_dim=2048)_AdamW.png){width=320px}
![](assets/img/ViT_(patch_size=4,_dim=1024,_mlp_dim=2048)_SGD.png){width=320px}

Even in case of `SGD` there is a hint of possible over-fitting as the validation loss is getting further from the train one.

Another interesting observation is that the original ViT network with parameters `patch_size=2`, `dim=32`, `mlp_dims=64` is the best and the worst of the comparable models at the same time, the only difference is the optimizer, when `SGD` being the worse one.
Furthermore, this network has the worst results of all the trained networks. Previously in the training section of this report given network's training graphs were compared, and it seemed that the `SGD` was simply too slow to converge.

### Average Time per Epoch

As the project's goal was to create a faster network, the average time per epoch was measured and evaluated.
Results are again displayed for all networks and then for networks which are a subject for comparison.

All models:  
![](assets/report/epoch_time.png){width=600px}  

Comparable models:  
![](assets/report/epoch_time(benchmark_only).png){width=600px}

In this case, the results look really promising for the custom models.
It is clear, that the `patch_size` parameter results in much more network in case of original ViT.
The interpretation could be that the smaller patch size results in more patches in general,
given that these patches are fed to the Transformer network, where multiple self attention computations occur.
This computation is much more demanding due to more possible *Q, K, V* values present.

On the other hand custom network with convolution and pooling scales the image down thus lowering the number of patches.

### Loss vs. Epoch Time

Scatter plots bellow might help to better understand tradeoffs of each created network:  
![](assets/report/loss_vs_epoch_time.png){width=700px}  
![](assets/report/loss_vs_epoch_time(benchmark_only).png){width=700px}

### Complete Results Table

| name            | patch size |  dim | mlp&nbsp;dim | optimizer |  loss | avg. epoch time (s) | epoch |
|-----------------|-----------:|-----:|-------------:|----------:|------:|--------------------:|------:|
| ViT             |          4 |   64 |          128 |     AdamW | 0.989 |                13.4 |    19 |
| Vit             |          2 |   32 |           64 |     AdamW | 1.006 |                81.4 |    27 |
| ConvVit  2xConv |          4 |   32 |           64 |     AdamW | 1.019 |                 3.8 |    29 |
| ConvVit  3xConv |          2 |   32 |           64 |     AdamW | 1.021 |                 3.7 |    29 |
| Vit             |          4 |   32 |           64 |     AdamW | 1.021 |                12.1 |    25 |
| Vit             |          4 |  128 |          256 |     AdamW | 1.032 |                15.9 |    12 |
| ConvVit  3xConv |          4 |   32 |           64 |     AdamW | 1.040 |                 3.7 |    30 |
| ConvVit  2xConv |          2 |   32 |           64 |     AdamW | 1.110 |                 5.8 |    30 |
| ConvVit  3xConv |          4 |   32 |           64 |       SGD | 1.188 |                 3.8 |    28 |
| ConvVit  3xConv |          2 |   32 |           64 |       SGD | 1.293 |                 3.8 |    30 |
| Vit             |          4 | 1024 |         2048 |       SGD | 1.311 |               104.2 |    29 |
| ConvVit  2xConv |          4 |   32 |           64 |       SGD | 1.359 |                 3.8 |    30 |
| ConvVit  2xConv |          2 |   32 |           64 |       SGD | 1.419 |                 5.8 |    30 |
| Vit             |          4 |  128 |          256 |       SGD | 1.685 |                15.9 |    30 |
| Vit             |          4 |   64 |          128 |       SGD | 1.789 |                13.4 |    30 |
| Vit             |          4 |   32 |           64 |       SGD | 1.803 |                12.2 |    30 |
| Vit             |          4 | 1024 |         2048 |     AdamW | 1.827 |               101.8 |     5 |
| Vit             |          2 |   32 |           64 |       SGD | 1.850 |                83.7 |    30 |

## Test Set Evaluation

As this project is focused on the possible improvement and comparison, two best networks, one from each category were be evaluated on the test set with the following results:

| name                | patch size | dim | mlp&nbsp;dim | optimizer | val. los | test set loss | checkpoint&nbsp;epoch |
|:--------------------|-----------:|----:|-------------:|----------:|---------:|--------------:|----------------------:|
| ViT                 |          4 |  64 |          128 |     AdamW |    0.989 |         1.006 |                    19 |
| ConvViT&nbsp;2xConv |          4 |  32 |           64 |     AdamW |    1.019 |         0.991 |                    29 |

The results seem a bit surprising, it would have been useful to have bigger validaiton/test set to be more confident. The difference between models is not that big and can probably be considered a random error.

## Summary

Multiple parameters of ViT network were explored and a total of 5 ViT networks was trained, furthermore 4 different custom networks based on ViT were created, trained, evaluated and compared to their original counterparts.
The results seem very promising as the networks showed similar performance while maintaining average epoch time lower, sometimes even significantly.

## Sources

| number | title                 | url                                                                                        |
|--------|-----------------------|--------------------------------------------------------------------------------------------|
| 1      | ViT&nbsp;Paper        | [https://openreview.net/pdf?id=YicbFdNTTy](https://openreview.net/pdf?id=YicbFdNTTy)       |
| 2      | ViT&nbsp;Pytorch      | [https://github.com/lucidrains/vit-pytorch](https://github.com/lucidrains/vit-pytorch)     |
| 3      | CIFAR-10&nbsp;dataset | [https://www.cs.toronto.edu/~kriz/cifar.html](https://www.cs.toronto.edu/~kriz/cifar.html) |
