# Milestone

## Úvod
Práce se zabývá experimentálním rošířením sítě Vision Transformer [článek 1] přídáním konvolučních vrstev před síť.
Samotný Vision Tranformer slouží ke klasifikaci obrázků do tříd a jak název napovídá využívá [článek 4]

## Rešerše modelů a článků
Vision Transformer z článeku [článek 1] se tématicky váže na model MLP-Mixer [článek 2] a tím pádem je vhodné prozkoumat oba články.
Autoři také model a postup trénování v dalším článku [článek 3] zjednodušili, čímž dosáhli zrychlení procesu trénování a inference.

## Datasety
V článcích [článek 1,2,3] byl použit dataset zmenšený ImageNet dataset ImageNet-1k [dataset 1], který je veřejně dostupný.
Dále byla použita novější plná verze tohoto datasetu z roku 2021, která vyžaduje registraci školním emailem [datasety 2, 3].
Dalšími datasety jsou pak CIFAR-10 [dataset 4] a CIFAR-100 [dataset 5], kde CIFAR-100 obsahuje konkrétnější třídy, ale velikostí vzorků je stejný,
jedná se o menší datasety, které by mohly více vyhovovat výkonostním omezením HW chudého studenta.
Bohužel rozlišení tohoto datasetu nevyhovuje cíli úlohy, protože je již tak příliš nízké. Z tohoto důvodu budou tyto datasety použity pouze pro ověření funkčnosti hlavní sítě.

## Implementace Vision Transformeru
Protože je cílem práce přidat konvoluční část před ViT síť, je možnost vytvoření vlastní implementace sítě, případně využití existující.
V každém případě existující implementace mohou pomoci podrobněji pochopit vnitřní strukturu síťě.
Původní implementace [impl 1] byla vytvořena společností Google a není tedy nic překvapivého, že bylo využito frameworku Tensorflow, případně JAX, které tato firma vytvořila.
Z bakalářské práce mám zkušenosti převážně se frameworkem PyTorch, který se mi jevil více přívětivý z pohledu ergonomie.
Z tohoto důvodu se i nyní kloním spíše k PytTorch implementaci, ale pro skutečné zhodnocení Tensorflow nemám dostatečné znalosti.

## Současný stav
Aktuální počátek implementace lze nalézt v souboru [main.ipynb](main.ipynb)

## Zdroje implementací
| číslo | název               | framework      | url                                                            |
|---|-------------------------|----------------|----------------------------------------------------------------|
| 1 | originální implementace | Tensorflow/JAX | https://github.com/google-research/vision_transformer          |
| 2 | PyTorch/TorchVision     | Pytorch        | https://pytorch.org/vision/main/models/vision_transformer.html |
| 3 | impl. od Phil Wang      | Pytorch        | https://github.com/lucidrains/vit-pytorch                      |

## Zdroje článků
| číslo | název                                      | url                                      |
|---|--------------------------------------------|------------------------------------------|
| 1 | článek modelu Vision Transformer (ViT)     | https://openreview.net/pdf?id=YicbFdNTTy |
| 2 | článek modelu MLP-Mixer                    | https://arxiv.org/abs/2105.01601         |
| 3 | Better plain ViT baselines for ImageNet-1k | https://arxiv.org/abs/2205.01580         |
|4| článek Attention Is All You Need | https://arxiv.org/abs/1706.03762         |
## Zdroje datasetů
| číslo | název                        | nutná registrace| vzorků |  #tříd | rozlišení | veliksot archivu | url                                                  |
|---|----------------------------------|------------------|-------:|-------:|----------:|-----------------:|------------------------------------------------------|
| 1 | ImageNet-1k/ILSVRC2017           | ne               |   1.2M |   1000 |  ~469x387 |            56 GB | https://www.image-net.org/challenges/LSVRC/index.php |
| 2 | ImageNet-21k                     | ano              |  14.2M | 21 841 |  ~469x387 |         1 104 GB | https://image-net.org/download-images.php            |
| 3 | Processed version of ImageNet21K | ano              | -\|\|- | -\|\|- | <~469x387 |           261 GB | https://image-net.org/download-images.php            |
| 4 | CIFAR-10                         | ne               | 60 000 |     10 |     32x32 |           161 MB | https://www.cs.toronto.edu/~kriz/cifar.html          |
| 5 | CIFAR-100                        | ne               | 60 000 |    100 |     32x32 |           163 MB | https://www.cs.toronto.edu/~kriz/cifar.html          |
